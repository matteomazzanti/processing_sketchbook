/* N.1 */
//import java.net.InetAddress;
//import java.net.UnknownHostException;
 
//static final String LOCAL_IP = findLanIp();
 
//void setup() {
//  println(LOCAL_IP);
//  exit();
//}
 
//static final String findLanIp() {
//  try {
//    return InetAddress.getLocalHost().getHostAddress();
//  }
//  catch (final UnknownHostException notFound) {
//    System.err.println("No LAN IP found!");
//    return "";
//  }
//}



/* N.2 */
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.net.SocketException;

String LOCAL_IP;

try {
  final DatagramSocket socket = new DatagramSocket();
  socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
  LOCAL_IP = socket.getLocalAddress().getHostAddress();
  println(LOCAL_IP);
  socket.close();
} catch ( UnknownHostException ex) {
  System.err.println("Error InetAddress. No LAN IP found!");
} catch ( SocketException ex ) {
  System.err.println("Error Socket. No LAN IP found!");
}

/* N. 3 */
