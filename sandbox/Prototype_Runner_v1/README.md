# Yet Another Neuro Runner Game! 

!["Screenshot"](neuro-runner_screenshot.png "screenshot")

Move the ball through the forest and collect speed points!

It is meant to be played with a brain frequency band power as input using a 
brain-computer interface.  


In an external software, a frequency power (amplitude of apha waves)
is mapped to a range value that goes from -1 to +1.
The values are received by this game via OSC protocol, on port 5000, on
the path ```/runner```.     
The ball will move and the points will be collected
proportionally to the brain frequency band power.

Video demo: https://www.youtube.com/watch?v=qhgJh8qux90

Neuromore: https://www.neuromore.com/
MindLAB Space: https://mindlab.space

**Source code and releases:** https://gitlab.com/matteomazzanti/processing_sketchbook/-/tree/develop/sandbox/Prototype_Runner_v1


**Disclaimer:** this software is silly and horribly coded. Use at your own risk.   
**License:** WTFPL   
**Author:** anonymous (I feel so shameful I did this)

