/* 
 * Yet Another Neuro Runner Game!
 * 
 * Move the ball through the forest and collect speed points!
 *
 * It is meant to be played with a brain frequency band power as input.
 * In an external software, a frequency power (amplitude of apha waves)
 * is mapped to a range value that goes from -1 to +1.
 * The values are received by this game via OSC protocol, on port 5000, on
 * the path /runner. The ball will move and the points will be collected
 * proportionally to the brain frequency band power.
 * 
 * Disclaimer: this software is silly and horribly coded. Use at your own risk.
 * License: WTFPL
 * Author: anonymous (I feel so shameful I did this)
 */

/* Imports */
import oscP5.*;
import processing.sound.*;


/* Init vars */
// OSC
OscP5 oscP5;
// background image
PImage background_image;
// background image x-axis position
float bk_image_x;
// move backgroud image at speed * moving_factor
float moving_factor;
// character image
PImage character_image;
// user's points (score!)
float score;
// timer
float time_left;
float tot_minutes = 0.80;

// game flow
boolean game_idle;
boolean game_over;

// music & sound
SoundFile file;
WhiteNoise noise;
LowPass lowPass;
float freq;



/* Initial setup */
void setup() {
  
  /* tests */
  String simplename = this.getClass().getSimpleName();
  println("I am " + simplename + "! \n Hello World!");
  
  /* draw display */
  size(640, 360);
  background(125,125,125);
  
  /* load media */
  // background
  background_image = loadImage("media/forest.jpg");
  background_image.resize(width, height);
  // initial backgrounf image position x
  bk_image_x = 0;
  // character
  character_image = loadImage("media/ball.png");
  character_image.resize(60, 60);
  
  // connaction to steam (OSC)
  // OSC inputs
  oscP5 = new OscP5(this,5000);
  oscP5.plug( this, "process_input_data", "/runner" );
  moving_factor = 0.03;

  // user's score
  score = 0;
  
  // timer
  time_left = (tot_minutes * 60 * 1000) + millis();
  
  // game flow
  game_idle = true;
  game_over = false;
  
  // sound
  file = new SoundFile(this, "media/Arcade-Puzzler.mp3");
  file.amp(0.8);
  noise = new WhiteNoise(this);
  lowPass = new LowPass(this);
  lowPass.process(noise);
  
}


/* Draw loop */
void draw() 
{
  draw_background_and_move_background();
  draw_character_speed_effect();
  draw_character();
  draw_score();
  draw_time_left();
  draw_game_over();
  draw_start_game_button();
  adjust_white_noise_filter();
}


public void process_input_data(float osc_data_01) {

  if (!game_over && !game_idle) {
    // println("### plug event method. received a message /runner.");
    //println(" 1 float received: " + osc_data_01);
    moving_factor = map(osc_data_01, -1,1, 0,1);
  } else {
    moving_factor = 0.03;
  }
}


public void draw_start_game_button() 
{
  if (game_idle) {
    rectMode(CENTER);
    fill(255, 255, 0);
    noStroke();
    rect(width/2, height/2 + 70, 120, 40, 5);
    
    String start_game_label = "start game";
    textAlign(CENTER);
    textSize(16);
    fill(255, 255, 255);
    text(start_game_label, width/2, height/2 + 75);
  }
}



public void draw_background_and_move_background() 
{
  imageMode(CORNER);
  bk_image_x = bk_image_x - (6 * moving_factor);
  if ( 0 > bk_image_x + 640 ) {
    bk_image_x = 0;
  }
  tint(255, 255);
  image(background_image, bk_image_x, 0);
  image(background_image, width + bk_image_x, 0);
}

public void draw_character() 
{
  imageMode(CENTER);
  tint(255, 180);
  image(character_image, width/2, height/2);
}

public void draw_character_speed_effect() 
{
    imageMode(CENTER);
    tint(255, 128);
    image(character_image, width/2 - (1 * 10 * moving_factor), height/2);
    tint(255, 96);
    image(character_image, width/2 - (1 * 20 * moving_factor), height/2);
    tint(255, 64);
    image(character_image, width/2 - (1 * 40 * moving_factor), height/2);
    tint(255, 32);
    image(character_image, width/2 - (1 * 60 * moving_factor), height/2);
}

public void draw_score()
{
  if ( !game_over && !game_idle)
  {
    score = score + abs(moving_factor / 1000);
  }
  
  textAlign(LEFT);
  textSize(32);
  fill(255, 255, 255);
  text(score, 40, 60);
  
  String score_label = "your score";
  textAlign(LEFT);
  textSize(16);
  fill(255, 255, 255);
  text(score_label, 55, 25);
  
}

public void draw_time_left()
{
  float time = 0;
  if ( !game_over && !game_idle ) {
    time = time_left - millis();
    
    // time is over
    if (0 >= time) {
      time = 0;
      //
      file.stop();
      noise.stop();
      moving_factor = 0.03;
      game_over = true;
      game_idle = true;
      println("GAME OVER! " + "Score: " + score);
    }
    
  }

  textAlign(RIGHT);
  textSize(32);
  fill(255, 255, 0);
  text(int(time), width - 50, 60);
  
  String time_left_label = "time left";
  textAlign(RIGHT);
  textSize(16);
  fill(255, 255, 0);
  text(time_left_label, width - 60, 25);
}

public void draw_game_over()
{
  if (game_over) {
    fill(219, 17, 219);
    textSize(46);
    textAlign(CENTER);
    text("GAME OVER", width/2, height/2 -50);
  }
}

public void adjust_white_noise_filter()
{
  println("I am called");
  freq = map(moving_factor, 0,1, 16,6000);
  lowPass.freq(freq);
}

/**
 * Check if the mouse is over a rectangle/button 
 */
public boolean over_button(int x, int y, int width, int height)  {
  if (mouseX >= x -width/2  && mouseX <= x+width/2 && 
      mouseY >= y -height/2 && mouseY <= y+height/2) {
    return true;
  } else {
    return false;
  }
}

void mousePressed() {
  boolean over_start = over_button(width/2, height/2 + 70, 120, 40);
  if ( over_start ) {
    if (game_idle) {
      // reset time
      time_left = (tot_minutes * 60 * 1000) + millis();
      // reset score
      score = 0;
      // start music
      file.play();
      
      noise = new WhiteNoise(this);
      lowPass = new LowPass(this);
      lowPass.process(noise);
      noise.play(0.3);
      
      
      game_idle = false;
      game_over = false;
      
      println("GAME START!");
    }
  }
}
