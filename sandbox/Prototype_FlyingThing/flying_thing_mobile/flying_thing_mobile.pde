
import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

Flying_thing ft;
float ft_new_y = 0;

void setup() {
  
  // Phone screen, 
  // it replaces: size(640, 360);
  fullScreen();

  
  background(125,125,125);
  
  
  /* OSC inputs */
  oscP5 = new OscP5(this,5000);
  //myRemoteLocation = new NetAddress("127.0.0.1",5000);
  oscP5.plug( this, "test", "/test" );
  

  // display a flying thing
  ft = new Flying_thing();
  
}

public void test(float theA) {
  println("### plug event method. received a message /test.");
  println(" 2 ints received: " + theA);  
  ft_new_y = map(theA, -1, 1, height, 0);
  ft.update(ft.x, ft_new_y);
}



void draw() {
  background(0,125,125);
  
  ft.checkEdges();
  ft.display();
  
}
