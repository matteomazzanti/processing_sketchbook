class Flying_thing
{
  float x;
  float y;
  float diameter;
  
  Flying_thing()
  {
    diameter = 60;
    x = width/2;
    y = height/2;
    println("we are on you!");
    println("new coord: " + str(x) + " " + str(y)); 
  }
  
  void update(float new_x,float new_y) 
  {
    x = new_x;
    y = new_y;
    //println("new coord:" + str(x) + str(y));  
  }
  
  void checkEdges() {
    checkCanvasBorders();
  }
  
  void display() {
    circle(x, y, diameter);
  }
  
  /**
   * Tested check.
   * If you see this logging, something went wrong.
   * the shape is out of the canvas borders.
   */
  void checkCanvasBorders() {
    if( x < 0 || x > width ) {
      println("OUT OF B-BORDER (width)!");
    }
    if(y < 0 || y > height ) {
      println("OUT OF B-BORDER (height)!");
    }
  }
  
}
