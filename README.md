# Processing

Processing Tryout: A sandbox project fro processing.   
Use it to try out things.   



## Usage

VSCode / processing integration is not optimal.  
Always use and edit the file ```processing_sketchbook.pde```.
Compile and run your sketch with ```Ctrl+Shift+B```.

Once done, copy-paste your sketch code into a sketch file 
and inside a sketch folder: 
```./sandbox/<sketch-name-folder>/<sketch-name-file>```   
where:
```<sketch-name-folder>``` and ```<sketch-name-file>``` MUST have the same name.

Doing so it will be possible to edit, compile and run sketches also inside 
the original processing editor.



## Visual Studio Code - Processing integration

https://marketplace.visualstudio.com/items?itemName=Tobiah.language-pde



### Useful commands

```Ctrl+Shift+B``` will compile and run your sketch. 


## Libraries
### OSC Processing integration

Library:  
http://www.sojamo.de/libraries/oscP5/